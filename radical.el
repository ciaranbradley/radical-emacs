(provide 'radical)

(require 'package)
(require 'cl-lib)

(cl-defmacro os-case (&body cases)
  `(cond ,@(cl-loop for case in cases collect
                     (if (eql (car case) t)
                         `(t ,@(cdr case))
                       `((eql system-type ',(car case)) ,@(cdr case))))))

;; Differ from portacle
;; this is an emacs.d install
(setq radical-root (expand-file-name "~/.emacs.d/radical/"))
(setq radical-os (os-case (gnu/linux "lin") (darwin "mac") (windows-nt "win")))

(defun radical-path (path)
  (concat radical-root path))

;;
;;
(require 'radical-package)
(require 'radical-keys)
(require 'radical-general)
(require 'radical-config)
(require 'radical-window)
(require 'radical-neotree)
(require 'radical-eshell)
(require 'radical-paredit)
(require 'radical-company)
;; Homebrew
(require 'radical-functions)
(require 'radical-magit)
(require 'radical-elfeed)
(require 'radical-fsharp)
(require 'radical-psql)
(require 'radical-fennel)
(require 'radical-clojure)

(when (window-system)
  (radical--setup-frame)
  (setq frame-title-format '("Radical")))
