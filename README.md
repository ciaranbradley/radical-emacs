## About Radical-Config

Radical is a collection of elisp files that configure a personal choice emacs instance. Heavily inspired by the Portacle portable common lisp IDE.

## How to launch

Add these lines to .emacs.d/init.el

;;Radical
(setq radical-root (expand-file-name "~/.emacs.d/radical/"))

(add-to-list 'load-path radical-root)

(cd radical-root)

(if (locate-library "radical")
    (load-library "radical")
  (display-warning :warning "Cannot load Radical."))

## How to customise

The theory is to avoid a "one massive init.el" file. Therefore if you wish to add functionality to a new mode, created a file for it. Set (provide 'foo) and add the config there, then add (require 'foo) the root radical.el file. The new config should then load on startup.

