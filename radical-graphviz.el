(provide 'radical-graphviz)

(ensure-installed 'use-package)
(ensure-installed 'graphviz-dot-mode)
(ensure-installed 'company-graphviz-dot)

(use-package graphviz-dot-mode
  :ensure t
  :config
  (setq graphviz-dot-indent-width 4))

(use-package company-graphviz-dot)
