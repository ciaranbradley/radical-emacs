(require 'browse-url)
(require 'cl-lib)
(require 'radical-package)
(require 'radical-keys)

(ensure-installed 'sublime-themes)

(setq inhibit-startup-screen t)
(setq pop-up-frame-function (lambda () (split-window-right)))
(setq split-height-threshold 1400)
(setq split-width-threshold 1500)
(setq browse-url-browser-function 'browse-url-generic)
(setq browse-url-generic-program (or (getenv "BROWSER") "xdg-open"))
(setq ring-bell-function 'ignore)

(defun radical--setup-frame ()
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (load-theme 'spolsky t)
  (toggle-frame-maximized)
  (setq confirm-kill-emacas 'y-or-n-p)
  (add-to-list 'default-frame-alist
               '(font . "Noto Mono-11:antialias=subpixel")))

(provide 'radical-window)
