;;
;;
;; Members Interests will take the name of an MP,
;;

(with-current-buffer
    (url-retrieve-synchronously
     "https://www.parliament.uk/mps-lords-and-offices/standards-and-financial-interests/parliamentary-commissioner-for-standards/registers-of-interests/register-of-members-financial-interests/")
  (switch-to-buffer (buffer-string)))

(buffer-size)

(defun get-url (url)
  "Get the data from a url and put contents in buffer"
  (url-retrieve
   url
   (lambda (result)
     (switch-to-buffer result))))

(setq parliament-url "https://www.parliament.uk/mps-lords-and-offices/standards-and-financial-interests/parliamentary-commissioner-for-standards/registers-of-interests/register-of-members-financial-interests/")
