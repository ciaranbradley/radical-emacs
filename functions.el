(defun radical-remove-ctrl-m ()
  "Remove all ^M characters from buffer"
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (while (re-search-forward "\n" nil t)
      (replace-match "\n"))))

