(provide 'radical-fsharp)

;;Install fsharp-mode
(ensure-installed 'fsharp-mode)

(setq inferior-fsharp-program "c:/Program Files/dotnet/sdk/5.0.103/FSharp/fsi.exe")
