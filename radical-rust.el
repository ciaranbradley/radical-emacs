(provide 'radical-rust)

(ensure-installed 'rust-mode)

(require 'rust-mode)

(define-key rust-mode-map (kbd "C-c C-c") 'rust-run)

(setenv "PATH" (concat (getenv "PATH") ":" (expand-file-name "~/../../.cargo/bin")))

(add-to-list 'exec-path (expand-file-name "~/../../.cargo/bin"))
