(provide 'radical-neotree)

(ensure-installed 'neotree)

(setq neo-smart-open nil)
(setq neo-autorefresh nil)
(setq neo-theme 'arrow)
(setq neo-window-width 40)

(define-radical-key "C-x d" 'neotree-show)
(define-radical-key "C-x e" 'neotree-hide)
