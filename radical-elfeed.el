(provide 'radical-elfeed)

(ensure-installed 'elfeed)

(ensure-installed 'elfeed-org)

(elfeed-org)

;; Optionally specify a number of files containing elfeed
;; configuration. If not set then the location below is used.
;; Note: The customize interface is also supported.
(setq rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org"))

