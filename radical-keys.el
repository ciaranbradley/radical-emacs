(provide 'radical-keys)

(defvar radical-keys-minor-mode-map (make-keymap) "radical-keys-minor-mode keymap.")
(defmacro define-radical-key (kbd func)
  `(define-key radical-keys-minor-mode-map (kbd ,kbd) ,func))

;; Activate
(define-minor-mode radical-keys-minor-mode
  "A minor mode to allow radical override major modes"
  t " Radical" 'radical-keys-minor-mode-map)

(radical-keys-minor-mode 1)

(defun radical-minibuffer-setup-hook () (radical-keys-minor-mode 0))
(add-hook 'minibuffer-setup-hook 'radical-minibuffer-setup-hook)

(defadvice load (after give-radical-keybindings-priority)
  "Try to ensure that radical keybindings always have priority."
  (if (not (eq (car (car minor-mode-map-alist)) 'radical-keys-minor-mode))
      (let ((radicalkeys (assq 'radical-keys-minor-mode minor-mode-map-alist)))
        (assq-delete-all 'radical-keys-minor-mode minor-mode-map-alist)
        (add-to-list 'minor-mode-map-alist radicalkeys))))
(ad-activate 'load)

(when (and (eq system-type 'darwin)
           window-system)
  (setq mac-option-modifier nil)
  (setq mac-command-modifier 'meta))
