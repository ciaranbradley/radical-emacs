;;; -*- lexical-binding: t -*-

;;
;;
;; Adds IELM hotkeys as outlined in
;; https://caiorss.github.io/Emacs-Elisp-Programming/Elisp_Programming.html
;;
;;
(require 'ielm)

(defun ielm/clear-repl ()
  "Clear current repl buffer."
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)
    (ielm-send-input)))

;;
;; Make M-RET evaluate the expression
;;
(define-key inferior-emacs-lisp-mode-map
  (kbd "M-RET")
  #'ielm-return)

(define-key inferior-emacs-lisp-mode-map
  (kbd "C-j")
  #'ielm-return)

(define-key inferior-emacs-lisp-mode-map
  (kbd "RET")
  #'electric-newline-and-maybe-indent)

(define-key inferior-emacs-lisp-mode-map
  (kbd "<up>")
  #'previous-line)

(define-key inferior-emacs-lisp-mode-map
  (kbd "<down>")
  #'next-line)

(define-key inferior-emacs-lisp-mode-map
  (kbd "C-c C-q")
  #'ielm/clear-repl)


