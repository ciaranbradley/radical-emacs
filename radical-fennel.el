(provide 'radical-fennel)

(ensure-installed 'fennel-mode)

(add-to-list 'auto-mode-alist '("\\.fnl\\'" . fennel-mode))

(add-hook 'fennel-mode-hook #'enable-paredit-mode)
